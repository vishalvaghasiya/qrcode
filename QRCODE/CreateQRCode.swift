//
//  CreateQRCode.swift
//  QRCODE
//
//  Created by KMSOFT on 03/03/17.
//  Copyright © 2017 KMSOFT. All rights reserved.
//

import UIKit



class CreateQRCode: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var imgQRCode: UIImageView!
    @IBOutlet weak var btnAction: UIButton!
    @IBOutlet weak var slider: UISlider!
    var qrcodeImage: CIImage!
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func performButtonAction(_ sender: Any) {
        if qrcodeImage == nil {
            if textField.text == "" {
                return
            }
        
            let data = textField.text?.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
            
            let filter = CIFilter(name: "CIQRCodeGenerator")
            
            filter?.setValue(data, forKey: "inputMessage")
            filter?.setValue("Q", forKey: "inputCorrectionLevel")
            
            qrcodeImage = filter?.outputImage
            textField.resignFirstResponder()
            
            btnAction.setTitle("Clear", for: UIControlState())
            displayQRCodeImage()
            
           // imgQRCode.image = UIImage(ciImage: qrcodeImage)
            
        }  else {
            imgQRCode.image = nil
            qrcodeImage = nil
            btnAction.setTitle("Generate", for: UIControlState())
        }
        textField.isEnabled = !textField.isEnabled
        slider.isHidden = !slider.isEnabled
    }
    
    @IBAction func changeImageViewScale(_ sender: Any) {
        imgQRCode.transform = CGAffineTransform(scaleX: CGFloat(slider.value), y: CGFloat(slider.value))
    }

    func displayQRCodeImage() {
        let scaleX = imgQRCode.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = imgQRCode.frame.size.height / qrcodeImage.extent.size.height
        
        let transformedImage = qrcodeImage.applying(CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        imgQRCode.image = UIImage(ciImage: transformedImage)
        
        
    }
    
    
  }
